import os
import requests
import subprocess
import sys
import shutil

bucket_prefix = " https://pflotran-ogs-supertests.s3.amazonaws.com/"
#bucket_file   = "super_tests.tar"

if (len(sys.argv)<3):
  raise Exception("superTestDLManager: please provide at least two arguments")
target_folder = sys.argv[1]
bucket_file   = sys.argv[2]

doclean = False
if (len(sys.argv)>3):
  arg3 = sys.argv[3]
  if (arg3 == "--clean"):
    doclean = True

print (" ")
print (" ")

def dld():
# 1: create target folder
  print ("creating directory " + target_folder + "...")
  #if (not os.path.exists(target_folder)):
    #os.mkdir(target_folder)

  if (os.path.exists(target_folder)):
    shutil.rmtree(target_folder)
  os.mkdir(target_folder)
  print ("...done")

# 2: go to target folder 
  os.chdir(target_folder)

# 3: download:
  url = bucket_prefix+bucket_file  
  print ("downloading " + url + "...")
  req = requests.get(url)
  open(bucket_file, "wb").write(req.content)
  print ("...done")

# 4: untar: 
  print ("extracting..." )
  subprocess.call(["tar", "-xf", bucket_file])
  print ("...done")

# 5: clean up: 
  os.remove(bucket_file)

def clean():
  print ("deleting directory " + target_folder + "...")
  shutil.rmtree(target_folder)
  print ("...done")


if (doclean):
  clean()
else:
  dld()

# CO2 Injection - quarter spot case
# injection in Mton
# watertab and non-zero salinity use only for salting out effect
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE GAS_WATER
      OPTIONS
        RESERVOIR_DEFAULTS
        ISOTHERMAL
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block

SUBSURFACE

#========================== discretization ===================================

GRID
  TYPE grdecl gw10_wtab_br.grdecl
END

#========================== regression ===================================
!skip
REGRESSION
  CELLS
    1
    300
  /
END
!noskip

#========================== times ============================================
TIME
 FINAL_TIME 500 d
 MAXIMUM_TIMESTEP_SIZE 30.d0 d
 INITIAL_TIMESTEP_SIZE 0.1 d
/

#========================== output options ===================================
skip
OUTPUT
  MASS_BALANCE_FILE
   PERIODIC TIMESTEP 10  
  END
  ECLIPSE_FILE
    PERIOD_SUM TIMESTEP 10
    PERIOD_RST TIMESTEP 1
    WRITE_DENSITY
  END
  LINEREPT
END
noskip
#=========================== fluid properties =================================

FLUID_PROPERTY 
 DIFFUSION_COEFFICIENT 4.04d-9
/

#=========================== EOS =================================
BRINE 1.328 MOLAL  ! S(ppm) = 72000

EOS WATER
  SURFACE_DENSITY 1050.0 kg/m^3
  WATERTAB
    DATA_UNITS
      PRESSURE Bar
      FVF m^3/m^3
      VISCOSITY cP
    END
    DATA
      TEMPERATURE 45.0
	100	1.0287414849	0.3512414504
	120	1.0279295744	0.3512414504
	140	1.0271183046	0.3512414504
	160	1.0263076751	0.3512414504
	180	1.0254976853	0.3512414504
	200	1.0246883349	0.3512414504
	220	1.0238796232	0.3512414504
	240	1.0230715497	0.3512414504
	260	1.022264114	0.3512414504
	280	1.0214573156	0.3512414504
	300	1.0206511539	0.3512414504
      END !end TEMP block
    END !end DATA
  END !end WATERTAB
END


EOS GAS
  SURFACE_DENSITY 1.87 kg/m^3
  CO2_DATABASE gwCO2.dat
END

#=========================== material properties ==============================
MATERIAL_PROPERTY mat1
  ID 1
  ROCK_DENSITY 2.60E3
  SPECIFIC_HEAT 0.920E3
  THERMAL_CONDUCTIVITY_DRY 2.51
  THERMAL_CONDUCTIVITY_WET 2.51
  CHARACTERISTIC_CURVES ch1
/
#=========================== saturation functions =============================
#GAS_WATER module requires: Pcwg, Krw, Krg

CHARACTERISTIC_CURVES ch1

 PC_WG CONSTANT
  CONSTANT_PRESSURE 0.d0 Pa
 END

 PERMEABILITY_FUNCTION_WAT BURDINE_BC
   LAMBDA 0.457
   ALPHA 5.1d-5
   WATER_RESIDUAL_SATURATION 0.3d0
   WATER_CONNATE_SATURATION 0.d0
 END

 PERMEABILITY_FUNCTION_GAS BURDINE_BC_SL
   LAMBDA 0.457
   ALPHA 5.1d-5
   GAS_RESIDUAL_SATURATION 0.0  
   LIQUID_RESIDUAL_SATURATION 0.3d0
   GAS_CONNATE_SATURATION 0.d0
 END

 !TEST

END

#=========================== regions ==========================================
REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20 
/
/

#=========================== wells ==================================

WELL_DATA injg
  WELL_TYPE GAS_INJECTOR
  BHPL       1000 Bar
  DIAMETER   0.5 m
  ! tonne can be defines as: tonne, ton, t
  ! Mega tonne can be defined as: Mton, Mt
  TARG_GM 0.365 Mt/year
  CIJK 1 1 1 1  
END

WELL_DATA prod
  WELL_TYPE PRODUCER
  DIAMETER 0.5 m
  BHPL     100   Bar
  TARG_WSV 10000 m^3/day
  CIJK 10 10 1 1  
END

#=========================== flow conditions ==================================

FLOW_CONDITION initial
  
  TYPE
    PRESSURE hydrostatic
   /

  PRESSURE 120.0 Bar
  DATUM_D  1200.0 m

  RTEMP 45 C

/


#=========================== condition couplers ==============================

INITIAL_CONDITION
FLOW_CONDITION initial
REGION all
END

#========================== stratigraphy couplers ============================
STRATA
REGION all
MATERIAL mat1
END

#======================== region names

END_SUBSURFACE

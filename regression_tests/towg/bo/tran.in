﻿!  This is a test of the EQUALS TRAN case
!  The vertical transmissibilities are set to 10 cp.rm3/(day.Bar)
!  The viscosity is 0.5 cP
!  Water is injected at 10 rm3/day at bottom of study
!  For each cell interface q=T/visc.deltaP
!  So deltaP = q.visc/T = 10*0.5/10 = 0.5 Bar
!  So the pressure difference should be the initial 90m of head (8.826 Bar)
!  plus 9 deltaP interfaces of 0.5 Bar => 4.5 + 8.8237 = 13.3237
!  As of May 20 : Initial top cell,bot cell = 100.49,109.316, diff  8.826
!                 End run top cell,bot cell = 50.408, 63.734, diff 13.326
!                 Darcy deltaP              =                       4.5 Bar

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE BLACK_OIL
      OPTIONS
       RESERVOIR_DEFAULTS
       ISOTHERMAL
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block

SUBSURFACE

#=========================== discretization ===================================

GRID
  TYPE grdecl tran.grdecl
END

#=========================== times ============================================

TIME
  START_DATE 1 Jan 2010
  FINAL_DATE 1 Jan 2011
  MAXIMUM_TIMESTEP_SIZE 30 d at 0. d
END

#=========================== output ===========================================

! Uncomment to see results

!OUTPUT
!  ECLIPSE_FILE
!    PERIOD_SUM TIMESTEP 1
!    PERIOD_RST TIMESTEP 1
!  END
!  LINEREPT
!END

#=========================== material properties ==============================

MATERIAL_PROPERTY formation
  ID 1
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.350d3
  SPECIFIC_HEAT 1.0d3
  THERMAL_CONDUCTIVITY_DRY 1.541d0
  THERMAL_CONDUCTIVITY_WET 1.541d0
  SOIL_COMPRESSIBILITY_FUNCTION QUADRATIC
  SOIL_COMPRESSIBILITY    4.35d-10 ! 1/Pa
  SOIL_REFERENCE_PRESSURE 1.0D5
  CHARACTERISTIC_CURVES ch1
/

#=========================== saturation functions =============================

CHARACTERISTIC_CURVES ch1

 KRW_TABLE swfn_table
 KRG_TABLE sgfn_table
 KRO ECLIPSE
   KROW_TABLE sof3_table1
   KROG_TABLE sof3_table2
 END

 ! Water rel. perm will be 1

 TABLE swfn_table
   PRESSURE_UNITS Pa
   SWFN
    0.12  0       0
    1.0   1.0     0
   /
 END

 TABLE sgfn_table
  PRESSURE_UNITS Pa
   SGFN
    0     0       0
    1.0   1.0     0
   /
 END

 TABLE sof3_table1
   SOF3
    0        0.0      0.0
    0.88     1.0      1.0
   /
 END

 TABLE sof3_table2
   SOF3
    0        0        0
    0.18     0        0
    0.28     0.0001   0.0001
    0.88     1        1    
   /
 END
/

#=========================== EOSs =============================================

EOS WATER
  SURFACE_DENSITY 1000      ! Incompressible
  DENSITY CONSTANT 1000
  VISCOSITY CONSTANT 0.5d-3 ! 0.5 cp
END

! Oil and gas properties do not matter: this is an all-water case

EOS OIL
  FORMULA_WEIGHT 800.0d0
  SURFACE_DENSITY 740.757 kg/m^3
  PVCO
    DATA_UNITS ! Metric in the Eclipse sense
      PRESSURE Bar           !Bar is default
      RS  m^3/m^3            !m^3/m^3 is default
      FVF m^3/m^3            !m^3/m^3 is default
      VISCOSITY cP           !cP default
      COMPRESSIBILITY 1/Bar  !1/Bar is default
      VISCOSIBILITY   1/Bar  !1/Bar is default
    END
    DATA
      TEMPERATURE 15.0
          1.0   0.178 1.0620  1.0400   0.0001985 0.001308
        500.5 531.473 2.3570  0.2030   0.0001985 0.001308
      END !end TEMP block
    END !endDATA
  END !end PVDO
  ENTHALPY QUADRATIC_TEMP ! h = c1*(t-t1) + 0.5*c2*(t-t2)
    TEMP_REF_VALUES   15.6d0  15.6d0    !t1, t2 [°C]
    TEMP_COEFFICIENTS 2.224D3 0.0d0  !c1, c2 [J/kg/Â°C]
  END !end Enthlapy
END !end EOS OIL

EOS GAS
  FORMULA_WEIGHT 16.04d0
  SURFACE_DENSITY 0.97052664 kg/m^3
  PVDG
    DATA_UNITS ! Metric in the Eclipse sense
      PRESSURE  Bar
      FVF       m^3/m^3
      VISCOSITY cP
    END
    DATA
      TEMPERATURE 15.0
          1.0 0.9357635 0.0080
        500.0 0.0021672 0.0470
      END !end TEMP block
    END !endDATA
    !specify temperature dependency for viscosity
  END !end PVDG
END

#=========================== regions ==========================================

REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20
/
/

#=========================== wells ==================================

! Injector on water rate - will be reservoir rate in this case
 
WELL_DATA injw
  WELL_TYPE WATER_INJECTOR
  RADIUS   0.25 m
  BHPL     1000    Bar
  TARG_WSV 10 m^3/day
  CIJK 1 1 1 1  
END

!  Producer on bhp will follow injector

WELL_DATA prod
  WELL_TYPE PRODUCER
  RADIUS    0.25 m
  BHPL      50   Bar
  TARG_WSV  1000 m^3/day
  CIJK 1 1 10 10
END

#=========================== flow conditions ==================================

FLOW_CONDITION initial_press

  TYPE
    PRESSURE hydrostatic
   /

  PRESSURE 100 Bar 
  DATUM_D  1000 m
  OGC_D    800 m   ! Reservoir is in the water zone
  OWC_D    900 m
  PCOG_OGC 0.0    Bar
  PCOW_OWC 0.0    Bar

  BUBBLE_POINT_TABLE ! Not used as no oil
   D_UNITS m  
   PRESSURE_UNITS Bar
   PBVD
     1000.0 276.8
     3000.0 276.8
   /
  END

  RTEMP 15.0 C
/

#=========================== condition couplers ==============================

INITIAL_CONDITION initial
FLOW_CONDITION initial_press
REGION all
/

REGRESSION

  CELLS
     1
    10
  /

END
#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL formation
END

END_SUBSURFACE

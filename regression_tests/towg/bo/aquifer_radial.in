﻿
! Test of radial aquifer

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE BLACK_OIL
      OPTIONS
       ANALYTICAL_JACOBIAN
       ISOTHERMAL
      /
    / ! end of subsurface_flow
  / ! end of process models
END  ! end simulation block

SUBSURFACE

#=========================== discretization ===================================

GRID
  TYPE grdecl aquifer_radial.grdecl
END

#=========================== times ============================================

TIME
  FINAL_TIME 2000 d
  INITIAL_TIMESTEP_SIZE 1.0d-1 d
  MINIMUM_TIMESTEP_SIZE 1.0D-10 d
  MAXIMUM_TIMESTEP_SIZE 50 d at 0. d
END

#=========================== output options ===================================

! Remove comments to get useful output

!OUTPUT
!  MASS_BALANCE_FILE
!   PERIODIC TIMESTEP 1  
!  END
!  ECLIPSE_FILE
!    PERIOD_SUM TIMESTEP 1
!    PERIOD_RST TIMESTEP 1
!    OUTFILE
!  END
!  LINEREPT
!END

#=========================== material properties ==============================

MATERIAL_PROPERTY formation
  ID 1
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.350d3
  SPECIFIC_HEAT 1.0d3
  THERMAL_CONDUCTIVITY_DRY 1.541d0
  THERMAL_CONDUCTIVITY_WET 1.541d0
  SOIL_COMPRESSIBILITY_FUNCTION QUADRATIC
  SOIL_COMPRESSIBILITY    1.0E-9
  SOIL_REFERENCE_PRESSURE 1.0D5
  CHARACTERISTIC_CURVES ch1
/

#=========================== saturation functions =============================

CHARACTERISTIC_CURVES ch1

 KRW_TABLE swfn_table
 KRG_TABLE sgfn_table
 KRO ECLIPSE
   KROW_TABLE sof3_table1
   KROG_TABLE sof3_table2
 END

 TABLE swfn_table
   PRESSURE_UNITS Pa
   SWFN
    0.12  0       0
    1.0   1.0     0  
   /
 END

 TABLE sgfn_table
  PRESSURE_UNITS Pa
   SGFN
    0     0       0
    0.02  0       0
    0.05  0.005   0
    0.12  0.025   0
    0.2   0.075   0
    0.25  0.125   0
    0.3   0.19    0
    0.4   0.41    0
    0.45  0.6     0
    0.5   0.72    0
    0.6   0.87    0
    0.7   0.94    0
    0.85  0.98    0
    1.0   1.0     0
   /
 END

 TABLE sof3_table1
   SOF3
    0        0.0      0.0
    0.88     1.0      1.0
   /
 END

 TABLE sof3_table2
   SOF3
    0        0        0
    0.18     0        0
    0.28     0.0001   0.0001
    0.38     0.001    0.001
    0.43     0.01     0.01
    0.48     0.021    0.021
    0.58     0.09     0.09
    0.63     0.2      0.2
    0.68     0.35     0.35
    0.76     0.7      0.7
    0.83     0.98     0.98
    0.86     0.997    0.997
    0.879    1        1
    0.88     1        1    
   /
 END
 !TEST 
/

#=========================== EOSs =============================================


EOS WATER
  SURFACE_DENSITY 1000
  DENSITY EXPONENTIAL 1000 100000 1.0D-9
  VISCOSITY CONSTANT 1.0d-3 ! 1 cp
END

EOS OIL
  FORMULA_WEIGHT 800.0d0
  SURFACE_DENSITY 740.757 kg/m^3
  PVCO
    DATA_UNITS ! Metric in the Eclipse sense
      PRESSURE Bar           !Bar is default
      RS  m^3/m^3            !m^3/m^3 is default
      FVF m^3/m^3            !m^3/m^3 is default
      VISCOSITY cP           !cP default
      COMPRESSIBILITY 1/Bar  !1/Bar is default
      VISCOSIBILITY   1/Bar  !1/Bar is default
    END
    DATA
      TEMPERATURE 15.0
         1.013529    0.178 1.0620  1.0400   0.0001985 0.001308
         18.250422  16.119 1.1500  0.9750   0.0001985 0.001308
         35.487314  32.059 1.2070  0.9100   0.0001985 0.001308
         69.961099  66.078 1.2950  0.8300   0.0001985 0.001308
        138.908669 113.277 1.4350  0.6950   0.0001985 0.001308
        173.382454 138.277 1.5000  0.6410   0.0001985 0.001308
        207.856239 165.640 1.5650  0.5940   0.0001985 0.001308
        276.803809 226.197 1.6950  0.5100   0.0001985 0.001308
        345.751379 288.178 1.8270  0.4490   0.0001985 0.001308
        621.541659 531.473 2.3570  0.2030   0.0001985 0.001308
      END !end TEMP block
    END !endDATA
  END !end PVDO
  ENTHALPY QUADRATIC_TEMP ! h = c1*(t-t1) + 0.5*c2*(t-t2)
    TEMP_REF_VALUES   15.6d0  15.6d0    !t1, t2 [°C]
    TEMP_COEFFICIENTS 2.224D3 0.0d0  !c1, c2 [J/kg/Â°C]
  END !end Enthlapy
END !end EOS OIL

EOS GAS
  FORMULA_WEIGHT 16.04d0
  SURFACE_DENSITY 0.97052664 kg/m^3
  PVDG
    DATA_UNITS ! Metric in the Eclipse sense
      PRESSURE  Bar
      FVF       m^3/m^3
      VISCOSITY cP
    END
    DATA
      TEMPERATURE 15.0
          1.013529 0.9357635 0.0080
         18.250422 0.0678972 0.0096
         35.487314 0.0352259 0.0112
         69.961099 0.0179498 0.0140
        138.908669 0.0090619 0.0189
        173.382454 0.0072653 0.0208
        207.856239 0.0060674 0.0228
        276.803809 0.0045534 0.0268
        345.751379 0.0036439 0.0309
        621.541659 0.0021672 0.0470
      END !end TEMP block
    END !endDATA
    !specify temperature dependency for viscosity
  END !end PVDG
END

#=========================== regions ==========================================

REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20
/
/

#=========================== wells ==================================

WELL_DATA prod1
  WELL_TYPE PRODUCER
  DIAMETER   0.5 m
  BHPL       300 Bar
  TARG_LSV 10000 m^3/day
  CIJK 5 5 1 1 
END

AQUIFER_DATA A1
  DEPTH     2550.0 m
  THICKNESS 100 m
  RADIUS    507.8 m
  PERM      500  mD
  COMPRESSIBILITY 2.0E-9 1/Pa
  POROSITY  0.1
  VISCOSITY 1 cP
  CONN_D 1 1 1 9 1 1 X-
  CONN_D 9 9 1 1 1 1 X+
  CONN_D 1 9 1 1 1 1 Y-
  CONN_D 1 9 9 9 1 1 Y+
END 

#=========================== flow conditions ==================================
FLOW_CONDITION initial_press

  TYPE
    PRESSURE hydrostatic
   /

  PRESSURE 330.0 Bar 
  DATUM_D  2505.0 m
  OGC_D    2400.0 m
  OWC_D    2450.0 m
  PCOG_OGC 0.0    Bar
  PCOW_OWC 0.0    Bar

  BUBBLE_POINT_TABLE
   D_UNITS m  
   PRESSURE_UNITS Bar
   PBVD
     1000.0 276.8
     3000.0 276.8
   /
  END

  RTEMP 15.0 C
/

#=========================== regression info ==================================

REGRESSION

  CELLS
    1
    81
  /

END

#=========================== condition couplers ==============================

INITIAL_CONDITION initial
FLOW_CONDITION initial_press
REGION all
/

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL formation
END

END_SUBSURFACE

﻿
! Mini version of SPE3 benchmark

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE COMP 9 EOS ! 9 Total, 8 hydrocarbon in the PR EoS
      OPTIONS
       ISOTHERMAL
       RESERVOIR_DEFAULTS
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block

SUBSURFACE

#=========================== discretization ===================================

GRID
  TYPE grdecl mspe3.grdecl
END

#=========================== times ============================================
TIME
  FINAL_TIME 15 y
  INITIAL_TIMESTEP_SIZE 5 d
  MINIMUM_TIMESTEP_SIZE 1.0D-10 d
  MAXIMUM_TIMESTEP_SIZE 0.25 y at 0. d
END

#=========================== output options ===================================

OUTPUT
 LINEREPT
END

#=========================== material properties ==============================

MATERIAL_PROPERTY formation
  ID 1
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.350d3
  SPECIFIC_HEAT 1.0d3
  THERMAL_CONDUCTIVITY_DRY 1.541d0
  THERMAL_CONDUCTIVITY_WET 1.541d0
  SOIL_COMPRESSIBILITY_FUNCTION QUADRATIC
  SOIL_COMPRESSIBILITY  5.80151E-10 ! 1/Pa
  SOIL_REFERENCE_PRESSURE 1.0D5
  CHARACTERISTIC_CURVES ch1
/

#=========================== saturation functions =============================

CHARACTERISTIC_CURVES ch1

 KRW_TABLE swfn_table
 KRG_TABLE sgfn_table
 KRO ECLIPSE
   KROW_TABLE sof3_table
   KROG_TABLE sof3_table
 END

 TABLE swfn_table
   PRESSURE_UNITS psi
   SWFN
     0.16 0     50
     0.20 0.002 32
     0.24 0.010 21
     0.28 0.020 15.5
     0.32 0.033 12.0
     0.36 0.049 9.2 
     0.40 0.066 7.0
     0.44 0.090 5.3
     0.48 0.119 4.2
     0.52 0.150 3.4
     0.56 0.186 2.7
     0.60 0.227 2.1
     0.64 0.277 1.7
     0.68 0.330 1.3
     0.72 0.390 1.0
     0.76 0.462 0.7
     0.80 0.540 0.5
     0.84 0.620 0.4
     0.88 0.710 0.3
     0.92 0.800 0.2
     0.96 0.900 0.1
     1.0  1.000 0.0
   /
 END

 TABLE sgfn_table
  PRESSURE_UNITS Pa
    SGFN
     0.0  0      0
     0.04 0.005  0
     0.08 0.013  0
     0.12 0.026  0
     0.16 0.040  0 
     0.20 0.058  0
     0.24 0.078  0
     0.28 0.100  0
     0.32 0.126  0
     0.36 0.156  0 
     0.40 0.187  0
     0.44 0.222  0
     0.48 0.260  0
     0.52 0.300  0
     0.56 0.348  0
     0.60 0.400  0
     0.64 0.450  0
     0.68 0.505  0
     0.72 0.562  0
     0.76 0.620  0
     0.80 0.680  0
     0.84 0.740  0
     1    1      0
    /
 END

 TABLE sof3_table
   SOF3
   ! So   Krow   Krog
     0.0  0      0
     0.04 0      0
     0.08 0      0
     0.12 0      0
     0.16 0      0 
     0.20 0      0
     0.24 0      0
     0.28 0.005  0.005
     0.32 0.012  0.012
     0.36 0.024  0.024 
     0.40 0.040  0.040
     0.44 0.060  0.060
     0.48 0.082  0.082
     0.52 0.112  0.112
     0.56 0.150  0.150
     0.60 0.196  0.196
     0.64 0.250  0.250
     0.68 0.315  0.315
     0.72 0.400  0.400
     0.76 0.513  0.513
     0.80 0.650  0.650
     0.84 0.740  0.740
     1    1      1
   /
 END
/

#=========================== EOSs =============================================

EOS WATER
  SURFACE_DENSITY  1004
  DENSITY CONSTANT 1004
  VISCOSITY CONSTANT 0.0007 ! 0.7 cp
END

EOS COMP
  PREOS
  GCOND
  CNAME      CO2N2   C1     C2     C3     C46    C7P1   C7P2   C7P3
  TCRIT K    215.2   186.6  305.4  369.9  396.2  572.5  630.2  862.6
  PCRIT Bar  53.903  45.6   46.20  42.55  34.35  25.93  16.92  8.61
  ZCRIT      0.283   0.224  0.285  0.281  0.274  0.260  0.260  0.260
  ACF        0.1325  0.013  0.098  0.152  0.234  0.332  0.495  0.833
  MW         36.01   16.04  30.07  44.10  67.28  110.9  170.9  282.1
  BIC 0.036
  BIC 0.05   0
  BIC 0.08   0       0
  BIC 0.1002 0.09281 0       0
  BIC 0.1    0       0.00385 0.00385 0
  BIC 0.1    0       0.00630 0.00630 0 0 
  BIC 0.1    0.1392  0.00600 0.00600 0 0 0
END
 
SEPARATOR_DATA
 FIELDSEP 1 815  psi 26.67 C
 FIELDSEP 2 315  psi 26.67 C 
 FIELDSEP 3 14.7 psi 15.56 C 
END

#=========================== wells ==================================

WELL_DATA injg
  WELL_TYPE GAS_INJECTOR
  CIJK_D 1 1 1 1  
  ZINJ PERCENT 5 78 8 5 3 1 0 0  
  BHPL     4000 psi
  TARG_GSV  133088.96 m^3/day
  TIME 10 y
  SHUT 
END

WELL_DATA prod
  CIJK_D  3 3 2 2
  WELL_TYPE PRODUCER
  BHPL      500 psi
  TARG_GSV  175564 m^3/day 
END

#=========================== flow conditions ==================================

EQUIL initial_press

  TYPE
    OIL_PRESSURE hydrostatic
  /

  OIL_PRESSURE 3550 psi
  DATUM_D  7500 ft
  OGC_D    7499.0 ft
  OWC_D    7500.0 ft
  PCOG_OGC 0.0    Bar
  PCOW_OWC 0.0    Bar

  ZMFVD ft frac
     6500.0 0.0315  0.6599 0.0869 0.0591 0.0967 0.0472 0.0153 0.0034
     8500.0 0.0315  0.6599 0.0869 0.0591 0.0967 0.0472 0.0153 0.0034
  END

  RTEMP 93.4 C
/


REGRESSION
  CELLS
    1 
    18
  /
END

END_SUBSURFACE

module PM_Misc_Aux_module

!------------------------------------------------------------------------------
! A module to handle near-miscible and miscible states in EoS modelling
!------------------------------------------------------------------------------

#include "petsc/finclude/petscsys.h"
#include "petsc/finclude/petscvec.h"
  use petscsys
  use PFLOTRAN_Constants_module
  use petscvec

  implicit none

  private

  PetscReal, parameter :: eps_6 = 1.0D-6

  public :: FormMiscMob, FormFim

  contains

  subroutine FormFim(fim,fimx,s,md,mdx,xmf,xmfx,ymf,ymfx,para, &
                     np,nc,nch,ndof,ipoil,ipgas,option)

!------------------------------------------------------------------------------
! Form the oil-gas 'miscibility' factor
! fim is really the immiscibility factor, with fim=0 for a fully miscible state
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Apr 2022
!------------------------------------------------------------------------------

  use Option_module

  implicit none

  PetscReal, intent(in)  :: s  (np)
  PetscReal, intent(in)  :: md (np),mdx (np,ndof)
  PetscReal, intent(in)  :: xmf(nc),xmfx(nc,ndof)
  PetscReal, intent(in)  :: ymf(nc),ymfx(nc,ndof)
  PetscReal, intent(out) :: fim    ,fimx(   ndof)
  PetscReal, intent(in)  :: para(nch)
  PetscInt , intent(in)  :: ipoil,ipgas,np,nc,nch,ndof
  type(option_type) :: option

  PetscReal :: stsx(ndof),stx(ndof)

  PetscInt  :: ich, ic
  PetscReal :: mdo_molpcc, mdg_molpcc, st, sts, stref, strefi

  PetscReal, parameter :: conv=1.0/1000.0D0

!--Initialise output values----------------------------------------------------

  fim  = 1.0
  fimx = 0.0

!--Set up ref. surf. tension---------------------------------------------------

  stref = option%miscible_str
  if( stref<=0.0D0 ) then
    stref = 10.0D0 ! Assume stref~10 dynes/cm (mN/m) if zero
  endif

!--Set up inverse ref. surf. tension-------------------------------------------
  
  strefi = 1.0/stref
  
!--Obtain surface tension if oil and gas present-------------------------------

  if( s(ipoil)>1.0e-10 .and. s(ipgas)>1.0e-10 ) then

!  Form McLeod-Sugden sum

    st   = 0.0
    stx  = 0.0

    sts  = 0.0
    stsx = 0.0

    mdo_molpcc = md(ipoil)*conv
    mdg_molpcc = md(ipgas)*conv

    do ich=1,nch
      ic = ich+1
      sts  = sts  + para(ich)*(  xmf (ic  )*mdo_molpcc &
                               - ymf (ic  )*mdg_molpcc)
      stsx = stsx + para(ich)*(  xmfx(ic,:)*mdo_molpcc &
                               - ymfx(ic,:)*mdg_molpcc &
                               + xmf (ic  )*mdx(ipoil,:)*conv &
                               - ymf (ic  )*mdx(ipgas,:)*conv )
    enddo

! Take 4th power

    st  =     sts**4.0D0
    stx = 4.0*sts**3.0D0*stsx

    fim  = st *strefi
    fimx = stx*strefi
    
    if( fim>1.0 ) then
      fim  = 1.0
      fimx = 0.0
    endif

  endif

end subroutine FormFim

! *************************************************************************** !

subroutine FormMiscMob(so, sg, sw, sox, sgx, swx, md, mdx, xmf, ymf, xmfx, ymfx, &
                       np, nc, nch, ndof, ipoil, ipgas, &
                       fim, fimx, pcog, pcow, pcogx, pcowx, krom, krgm, kromx, krgmx, &
                       po, pg, pw, pox, pgx, pwx, para, &
                       characteristic_curves,table_idx, option)

!------------------------------------------------------------------------------
! Form the miscible relative permeabilities, cap, pressures and set phase pressures
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Apr 2022
!------------------------------------------------------------------------------

  use Characteristic_Curves_module 
  use Characteristic_Curves_OWG_module 
  use Option_module
  use Derivatives_utilities_module

  implicit none

  PetscReal, intent(in)  :: so,sg,sw,sox(ndof),sgx(ndof),swx(ndof)
  PetscReal, intent(in)  :: md(np),mdx(np,ndof),xmf(nc),ymf(nc),xmfx(nc,ndof),ymfx(nc,ndof)
  PetscInt , intent(in)  :: ipoil,ipgas,np,nc,nch,ndof
  PetscReal, intent(in)  :: fim, fimx(ndof),para(nch)
  PetscReal, intent(inout) :: pcog, pcow, pcogx(ndof),pcowx(ndof)
  PetscReal, intent(out) :: krom, krgm 
  PetscReal, intent(out) :: kromx(ndof), krgmx(ndof) 
  PetscReal, intent(inout) :: po, pg, pw, pox(ndof), pgx(ndof), pwx(ndof)

  class(characteristic_curves_type) :: characteristic_curves
  PetscInt, pointer :: table_idx(:)
  type(option_type) :: option

  PetscReal :: dsox(ndof),dsgx(ndof),wolx(ndof),wovx(ndof),wglx(ndof),wgvx(ndof)
  PetscReal :: krog, krgo
  PetscReal :: krol, krov, krgl, krgv, &
               krowl,krowv,krowlso,krowvso, &
               krgwl,krgwv,krgwlsg,krgwvsg, &
               krowlx(ndof),krowvx(ndof), &
               krgwlx(ndof),krgwvx(ndof), &
               krogm,krgom, sog, sogi, fm, &
               krogmx(ndof),krgomx(ndof), &
               krolx(ndof),krovx(ndof), &
               krglx(ndof),krgvx(ndof),sogx(ndof),fmx(ndof), &
               pcogrock,pcogrockx(ndof)

  PetscReal, parameter :: conv=1.0/1000.0D0

  PetscInt  :: ic,ich,icx
  PetscReal :: dsl,dsv,dso,dsg,mdo,mdg,wol,wov,wgl,wgv,sogcr,sgocr, &
               krogsl,krgosl,krogslx(ndof),krgoslx(ndof),krogx(ndof),krgox(ndof), &
               swcr,dummy1,dummy2,swco,somax,sgmax,krogmax,krgomax, &
              dummy,pc, ph, phx(ndof)

!--Extract molar density values-------------------------------------------------

  mdo = md(ipoil)
  mdg = md(ipgas)

!--Values for reference liquid and vapour--------------------------------------
 
!  Form McLeod-Sugden contributions/descriminants for ref. liquid and vapour
!  taken as pure decane and pure methane

  dsl = 431.0D0*mdo*conv ! C10 parachor used 
  dsv =  71.0D0*mdg*conv ! C1  parachor used

!--Values for actual oil and gas-----------------------------------------------

  dso = 0.0;dsox = 0.0
  dsg = 0.0;dsgx = 0.0

  do ich=1,nch

    ic  = ich + 1
    icx = ich + 1

    pc = para(ich)*conv

    dso  = dso  + pc* mdo*xmf(ic)
    dsg  = dsg  + pc* mdg*ymf(ic)

    dsox = dsox + pc*(mdx(ipoil,:)*xmf(ic) + mdo*xmfx(ic,:))
    dsgx = dsgx + pc*(mdx(ipgas,:)*ymf(ic) + mdg*ymfx(ic,:))

  enddo

!--Form the oil and gas weights------------------------------------------------

  call GetOGWeights(dso,dsox,wol,wolx,wov,wovx,ndof)
  call GetOGWeights(dsg,dsgx,wgl,wglx,wgv,wgvx,ndof)

!--Get the relative permeabilities---------------------------------------------

  ! Get required end-point values (connate water, critical oil-gas system vals)

  call characteristic_curves%GetOWGCriticalAndConnateSats(swcr,sgocr,dummy1, &
                      dummy2,sogcr,swco,option)

  ! Form the maximum hydrocarbon saturations as 1-(connate water)

  somax = 1.0-swco
  sgmax = 1.0-swco

  ! Gas-in-oil maximum rel perm

  call characteristic_curves%gas_rel_perm_func_owg% &
        RelativePermeability(sgmax,krgomax,dummy ,option,table_idx)

  ! Oil-in-gas and oil/gas-in-water rel perms

  select type(rpf => characteristic_curves%oil_rel_perm_func_owg)
    class is(rel_perm_oil_owg_ecl_type)

     ! Oil-in-gas maximum rel perm

      call rpf%GetKrog(somax,krogmax,dummy ,option,table_idx)

      ! For the phase identified as oil, find its liquid and vapour-like Krhw values
 
      call rpf%GetKrow(so,krowl,   krowlso,option,table_idx)
                          krowlx = krowlso*sox

      call rpf%GetKrgw(so,krowv,   krowvso,option,table_idx)
                          krowvx = krowvso*sox

      ! For the phase identified as gas, find its liquid and vapour-like Krhw values

      call rpf%GetKrow(sg,krgwl,   krgwlsg,option,table_idx)
                          krgwlx = krgwlsg*sgx

      call rpf%GetKrgw(sg,krgwv,   krgwvsg,option,table_idx)
                          krgwvx = krgwvsg*sgx

  end select

!--Do oil-gas miscible mixing on Krog and Krgo--------------------------------

  fm  = 1.0 - fim
  fmx =     - fimx
  if( fm<0.0 ) then
    fm  = 0.0
    fmx = 0.0
  endif

!--Get the rock oil-gas curves, scaled to end points multiplied by fim---------

  call ScaledRockRelPerms(so,sg,sox,sgx,sogcr,sgocr,swco,fim,fimx, &
                          krog,krogx,krgo,krgox, &
                          characteristic_curves,table_idx,option,ndof )

!--Get straight-line oil-gas curves, scaled to end points multiplied by fim----

  krogsl = GetStraightLineRelPerm(so,sox,sogcr,sgocr,swco,fim,fimx, &
                                  krogmax,krogslx,ndof )

  krgosl = GetStraightLineRelPerm(sg,sgx,sgocr,sogcr,swco,fim,fimx, &
                                  krgomax,krgoslx,ndof )

!--Get the miscibility-mixed oil-in-gas relative permeability curve-------------

  krogm  = fim*krog + fm*krogsl

  krogmx = ProdRule(fim,fimx,krog  ,krogx  ,ndof) + &
           ProdRule(fm ,fmx ,krogsl,krogslx,ndof)

!--Get the miscibility-mixed gas-in-oil relative permeability curve-------------

  krgom  = fim*krgo + fm*krgosl

  krgomx = ProdRule(fim,fimx,krgo  ,krgox  ,ndof) + & 
           ProdRule(fm ,fmx ,krgosl,krgoslx,ndof) 

!--Form the limiting (l and v) values for Kro and Krg--------------------------

  krol = Get3pRelPerm(sg,sw,swco,krogm,krogmx,krowl,krowlx,sgx,swx,krolx,ndof)
  krov = Get3pRelPerm(sg,sw,swco,krogm,krogmx,krowv,krowvx,sgx,swx,krovx,ndof)

  krgl = Get3pRelPerm(so,sw,swco,krgom,krgomx,krgwl,krgwlx,sox,swx,krglx,ndof)
  krgv = Get3pRelPerm(so,sw,swco,krgom,krgomx,krgwv,krgwvx,sox,swx,krgvx,ndof)

!--Interpolate between the liquid and vapour versions--------------------------

  krom  =   wol*krol &
          + wov*krov
  krgm  =   wgl*krgl &
          + wgv*krgv

  kromx =   ProdRule(wol,wolx,krol,krolx,ndof) &
          + ProdRule(wov,wovx,krov,krovx,ndof)

  krgmx =   ProdRule(wgl,wglx,krgl,krglx,ndof) &
          + ProdRule(wgv,wgvx,krgv,krgvx,ndof)

!--Modify the oil-gas cap pressures for miscibility--------------------------

  pcogrock  = pcog
  pcogrockx = pcogx

  pcog  = pcogrock *fim
  pcogx = pcogrockx*fim + pcogrock*fimx

!--Reset the phase pressures-------------------------------------------------

  sog  = so  + sg
  sogx = sox + sgx

  sogi = 0.0
  if( sog>0.0 ) sogi=1.0d0/sog

  ! Let pressure (currently in po) be ph (hydrocarbon pressure)
  ! This follows from: po = ph - (sg/sog)*pcog
  !                    pg = ph + (so/sog)*pcog
  ! Then:
  ! pg-po = (so-(-sg))/sog*pcog = sog/sog/pcog = pcog 
  ! and:
  ! pw = ph - pcow

  ph  = po
  phx = pox

  po  = ph  - sg*sogi*pcog 
  pox = phx - sg*sogi*pcogx - (sgx - sg*sogi*sogx)*sogi*pcog

  pg  = ph  + so*sogi*pcog
  pgx = phx + so*sogi*pcogx + (sox - so*sogi*sogx)*sogi*pcog

  pw  = ph  - pcow
  pwx = phx - pcowx

end subroutine FormMiscMob

! *************************************************************************** !

subroutine GetOGWeights(ds,dsx,wl,wlx,wv,wvx,ndof)

!------------------------------------------------------------------------------
! Form the liquid and vapour weights of a given oil or gas phase with
! phase discriminant value ds and derivatives dsx
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Apr 2022
!------------------------------------------------------------------------------

  implicit none

  PetscReal, intent(in)  :: ds, dsx(ndof)
  PetscReal, intent(out) :: wl, wlx(ndof)
  PetscReal, intent(out) :: wv, wvx(ndof)
  PetscInt , intent(in)  :: ndof

!--Initialise output values-----------------------------------------------------

  wl  = 0.5
  wlx = 0.0

!--Set up output values for the liquid weight----------------------------------

  if( ds>1.5 ) then
    ! Discriminant is >1.5 (water ~2) so it's an oil-like liquid
    wl=1.0
  else if( ds<0.5 ) then
    ! Discriminant is <0.5 (CH4   ~0.2) so it's a gas-like vapour
    wl=0.0
  else
    ! Is in an interpolated state between 0 (ds=0.5) and 1 (ds=1.5)
    wl  = ds-0.5
    wlx = dsx
  endif

!--Set up the vapour weight as the complement of the liquid weight-------------

  wv  = 1.0-wl
  wvx =    -wlx

end subroutine GetOGWeights

! *************************************************************************** !

function Get3pRelPerm(sb,sw,swco,krab,krabx,kraw,krawx,sbx,swx,krax,nx)

!------------------------------------------------------------------------------
! Get the hydrocarbon three-phase rel. perm. Kaw(Sa;Sb,Sw)
! If a is oil, b is gas
! If a is gas, b is oil
! The input is (Sb,Sw)
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Apr 2022
!------------------------------------------------------------------------------

  implicit none

  PetscReal :: Get3prelperm
  PetscReal,intent(in ) :: sb,sw,swco,krab,krabx(nx),kraw,krawx(nx), &
                           sbx(nx),swx(nx)
  PetscReal,intent(out) :: krax(nx)
  PetscInt ,intent(in ) :: nx

  PetscReal :: num,numx(nx),den,deni,deni2,denx(nx),kra,swc

!--Saturation-related denominator----------------------------------------------

  swc = sw-swco
  den = sb+swc

  if( den>eps_6 ) then

    ! Weight by other two saturations

    denx  = sbx+swx

    deni  = 1.0/den
    deni2 = deni*deni

    num  = sb *krab  + swc*kraw
    numx = sb *krabx + swc*krawx + &
          +sbx*krab  + swx*kraw    

    kra  =  num *deni
    krax =  numx*deni - num*deni2*denx

  else

    ! Simple constant weights

    kra  = 0.5*krab  + 0.5*kraw
    krax = 0.5*krabx + 0.5*krawx 

  endif

  Get3prelperm = kra

end function Get3prelperm

! *************************************************************************** !

subroutine ScaledRockRelPerms(so,sg,sox,sgx,sogcr,sgocr,swco,fim,fimx, &
                              krog,krogx,krgo,krgox, &
                              characteristic_curves,table_idx,option,nx )

!------------------------------------------------------------------------------
! Find the oil-in-gas and gas-in-oil rock curve rel. perm.
! scaled to the miscible curve end-points
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Apr 2022
!------------------------------------------------------------------------------

  use Characteristic_Curves_module 
  use Characteristic_Curves_OWG_module
  use Option_module

  implicit none

  PetscReal,intent(in)    :: so,sg,sox(nx),sgx(nx),sogcr,sgocr,swco,fim,fimx(nx)
  PetscReal,intent(inout) :: krog,krogx(nx),krgo,krgox(nx)
  class(characteristic_curves_type) :: characteristic_curves
  PetscInt, pointer :: table_idx(:)
  type(option_type) :: option
  PetscInt,intent(in)     :: nx

  PetscReal :: sol,uol,sou,uou,uo
  PetscReal :: sgl,ugl,sgu,ugu,ug
  PetscReal :: kroguo,krgoug
  PetscReal :: solim,souim,sglim,sguim,duodso,duodim,dugdsg,dugdim

!--Form the unscaled (lookup) and scaled (actual) endpoints--------------------

  uol   = sogcr
  sol   = sogcr*fim
  solim = sogcr

  uou   = 1.0-swco-sgocr
  sou   = 1.0-swco-sgocr*fim
  souim =         -sgocr

  ugl   = sgocr
  sgl   = sgocr*fim
  sglim = sgocr

  ugu   = 1.0-swco-sogcr
  sgu   = 1.0-swco-sogcr*fim
  sguim =         -sogcr

!--Form the unscaled (lookup) saturations)--------------------------------------

  call FormUnscaledSat(so, sol, sou, solim, souim, uol, uou, uo, duodso, duodim)
  call FormUnscaledSat(sg, sgl, sgu, sglim, sguim, ugl, ugu, ug, dugdsg, dugdim)

!--Do the lookups on the unscales functions-------------------------------------

  select type(rpf => characteristic_curves%oil_rel_perm_func_owg)
    class is(rel_perm_oil_owg_ecl_type)
      call rpf%GetKrog(uo,krog,kroguo,option,table_idx)
  end select

  call characteristic_curves%gas_rel_perm_func_owg% &
        RelativePermeability(ug,krgo,krgoug,option,table_idx)

!--Rescale the derivatives back to actual (saturation) space-------------------

  krogx = kroguo*(duodso*sox + duodim*fimx) 
  krgox = krgoug*(dugdsg*sgx + dugdim*fimx)

end subroutine ScaledRockRelPerms

! *************************************************************************** !

function GetStraightLineRelPerm( sh,shx,shcr,socr,swco,fim,fimx, &
                                 krhmax,krhslx,nx )

!------------------------------------------------------------------------------
! Get the hydrocarbon (oil or gas) straight-line rel perm curves from (0,0) to
! (1.0,krhmax), scaled to the miscible curve end-points shcr*fim and 1-socr*fim
! where socr is the critical saturation of the ohter phase 
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Apr 2022
!------------------------------------------------------------------------------

  implicit none

  PetscReal :: GetStraightLineRelPerm
  PetscReal,intent(in)  :: sh,shx(nx),shcr,socr,swco,fim,fimx(nx)
  PetscReal,intent(in)  :: krhmax
  PetscReal,intent(out) :: krhslx(nx)
  PetscInt ,intent(in)  :: nx

  PetscReal :: krhsl, shcrm, socrm, shmax, num, den, deni, numx(nx),denx(nx), &
               shcrmx(nx),socrmx(nx),shmaxx(nx)

!--Find 'this' hydrocarbon phase and 'other' hydrocarbon phase criticals--------

  shcrm  = shcr*fim
  shcrmx = shcr*fimx

  socrm  = socr*fim
  socrmx = socr*fimx

!--Find hydrocabon phase maximum-----------------------------------------------

  shmax  = 1.0-socrm -swco
  shmaxx =    -socrmx

!--Construct the hydrocarbon phase relative perm-------------------------------

  ! Default return values

  krhsl  = 0.0
  krhslx = 0.0

  if( sh<shcrm ) then

    ! Zero below critical

    krhsl = 0.0

  else if (sh>shmax ) then

   ! Max. value above critical

    krhsl = krhmax
  else

!   kh=khmax*(sh-shcrm)/(shmax-shcrm) between above values

    num    = krhmax*(sh -shcrm )
    numx   = krhmax*(shx-shcrmx)
    den    = shmax -shcrm
    denx   = shmaxx-shcrmx
    deni   = 1.0/den

    krhsl  =  num*deni
    krhslx = (numx - num*deni*denx)*deni

  endif

  GetStraightLineRelPerm = krhsl

end function GetStraightLineRelPerm

! *************************************************************************** !

subroutine FormUnscaledSat(s, sl, su, slm, sum, ul, uu, u, duds, dudm)

!------------------------------------------------------------------------------
! Given a saturation s, with scaling points sl and su, do lookup into an
! unscaled function, those points having been transformed to ul and uu   
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Apr 2022
!------------------------------------------------------------------------------

  implicit none

  PetscReal,intent(in) :: s, sl, su, slm, sum, ul, uu
  PetscReal,intent(out) :: u, duds, dudm
  PetscReal :: di,r,rm

!--Initial default return values-----------------------------------------------

  u    = s
  duds = 0.0
  dudm = 0.0

!--Map depends on interval----------------------------------------------------- 

  if( s<0.0 ) then
    ! Below 0.0: map into 0.0
    u = 0.0
  else if( s>1.0 ) then
    ! Above 1.0: map into 1.0
    u = 1.0
  else if( s<sl ) then
    ! Below sl: map into 0 to ul
    di   =  1.0/sl
    r    =  ul*di
    rm   = -ul*di*di*slm
    u    =  s*r
    duds =    r
    dudm =  s*rm
  else if( s>su ) then
    ! Above su: map into uu to 1
    di   =  1.0/(1.0-su)
    r    =  (1.0-uu)*di
    rm   =  (1.0-uu)*di*di*sum
    u    =  uu + (s-su)*r
    duds =              r
    dudm =       (s-su)*rm - sum*r
  else
    ! Between sl and su: map into ul to uu
    di   =  1.0/(su-sl)
    r    =  (uu-ul)*di 
    rm   = -(uu-ul)*di*di*(sum-slm) 
    u    =  ul + (s-sl)*r
    duds =              r
    dudm =       (s-sl)*rm - slm*r 
  endif

end subroutine FormUnscaledSat

end module PM_Misc_Aux_module

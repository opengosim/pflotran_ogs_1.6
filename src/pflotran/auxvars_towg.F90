module AuxVars_TOWG_module

  use PFLOTRAN_Constants_module

  use AuxVars_Base_module
  use petscsys

  implicit none
  
  private 

#include "petsc/finclude/petscsys.h"

!#define tl_deriv_tests
!#define tl3p_deriv_tests
!#define gw_deriv_tests
  PetscInt, parameter, public :: NPH_MAX = 4

  PetscBool, public :: towg_analytical_derivatives = PETSC_FALSE
  PetscBool, public :: towg_analytical_derivatives_compare = PETSC_FALSE

  PetscReal, public :: towg_dcomp_tol = 1.d-1
  PetscReal, public :: towg_dcomp_reltol = 1.d-1

  PetscInt, parameter, public :: NPHASE_TOWG = 3
  PetscInt, parameter, public :: NFLOWSPEC_TOWG = 3    !H20, Oil, Gas
  PetscInt, parameter, public :: NFLOWDOF_TOWG = 4

  PetscInt, parameter, public :: NPHASE_TOWG_GW = 2
  PetscInt, parameter, public :: NFLOWSPEC_TOWG_GW = 2  !H20, Gas
  PetscInt, parameter, public :: NFLOWDOF_TOWG_GW = 3
  
  PetscInt, parameter, public :: NPHASE_TOWG_TL = 4
  PetscInt, parameter, public :: NFLOWSPEC_TOWG_TL = 4 !H20, Oil, Gas, Solvent
  PetscInt, parameter, public :: NFLOWDOF_TOWG_TL = 5

  ! Maximum NFLOWDOF_TOWG value over the above modes
  PetscInt, parameter, public :: MFLOWDOF_TOWG = 5

  type, public, extends(auxvar_base_type) :: auxvar_towg_type  
    PetscInt :: istate_store(2) ! 1 = previous timestep; 2 = previous iteration
    type(tl_auxvar_type), pointer :: tl=>null()
    type(bo_auxvar_type), pointer :: bo=>null()
    type(co_auxvar_type), pointer :: co=>null()
    type(gw_auxvar_type), pointer :: gw=>null()
    type(hy_auxvar_type), pointer :: hy=>null()

#ifdef tl3p_deriv_tests
    PetscBool :: hastl3p_test_object
    type(tl3_test_type), pointer  :: tl3TEST=>null()
#endif

#ifdef tl_deriv_tests
    PetscBool :: has_TL_test_object
    type(tl_auxvar_testing_type), pointer :: tlT=>null()
#endif

#ifdef gw_deriv_tests
    PetscBool :: has_GW_test_object
    type(gw_auxvar_testing_type), pointer :: gwT=>null()
#endif

  contains
    procedure, public :: Init => AuxVarTOWGInit
    procedure, public :: Strip => AuxVarTOWGStrip
    procedure, public :: InitTL
    procedure, public :: StripTL
    procedure, public :: InitBO
    procedure, public :: InitCO
    procedure, public :: StripBO
    procedure, public :: StripCO
    procedure, public :: InitGW
    procedure, public :: StripGW
    procedure, public :: InitHY
    procedure, public :: StripHY

  end type auxvar_towg_type

  type, public ::  tl_auxvar_type
    PetscReal :: den_oil_eff_kg
    PetscReal :: den_gas_eff_kg
    PetscBool :: has_derivs
    PetscReal :: D_den_oil_eff_kg(MFLOWDOF_TOWG)   ! (idof)
    PetscReal :: D_den_gas_eff_kg(MFLOWDOF_TOWG)   ! (idof)
  end type tl_auxvar_type

  type, public :: bo_auxvar_type
    PetscReal :: bubble_point
    PetscReal :: xo
    PetscReal :: xg

    PetscBool :: has_derivs
    ! derivatives:
    PetscReal :: D_xo(MFLOWDOF_TOWG)   ! (idof)
    PetscReal :: D_xg(MFLOWDOF_TOWG)   ! (idof)
  end type bo_auxvar_type

  type, public :: co_auxvar_type

    PetscReal :: v
    PetscReal :: w

    PetscReal :: mp_mdo, mp_mdg, mp_kgdo, mp_kgdg

    PetscReal, allocatable :: xmf(:)
    PetscReal, allocatable :: ymf(:)
    PetscReal, allocatable :: amf(:)

    PetscReal, allocatable :: zmf(:)

    PetscReal, allocatable :: D_xmf(:,:)
    PetscReal, allocatable :: D_ymf(:,:)
    PetscReal, allocatable :: D_amf(:,:)

    PetscReal              ::   diffg
    PetscReal, allocatable :: D_diffg(:)

  end type co_auxvar_type

  type, public ::  gw_auxvar_type
    PetscReal :: xmol(NFLOWSPEC_TOWG_GW,NPHASE_TOWG_GW) ! (icomp,iphase)
    PetscReal :: diff(NFLOWSPEC_TOWG_GW,NPHASE_TOWG_GW) ! (icomp,iphase)

    PetscReal :: pc_dkp(NPHASE_TOWG_GW) ! (iphase)

    PetscBool :: has_derivs
    ! derivatives
    PetscReal :: D_xmol(NFLOWSPEC_TOWG_GW,NPHASE_TOWG_GW,NFLOWDOF_TOWG_GW) ! (icomp,iphase,idof)
    PetscReal :: D_diff(NFLOWSPEC_TOWG_GW,NPHASE_TOWG_GW,NFLOWDOF_TOWG_GW) ! (icomp,iphase,idof)

    PetscReal :: D_pc_dkp(NPHASE_TOWG_GW,NFLOWDOF_TOWG_GW) ! (iphase,idof)    
  end type gw_auxvar_type

  ! Hysteresis data structure
  type, public ::  hy_auxvar_type
    ! Turnaround saturation for the non-wetting phase
    PetscReal :: spnmax
    ! Shift in Carlson treatment of non-wetting rel. perm. hyst. lookup
    PetscReal :: spnshft
    ! Shift in Carlson treatment of wetting rel. perm. hyst. lookup
    PetscReal :: spwshft
    ! Trapped non-wetting saturation in hysteresis case.
    PetscReal :: spntrap
    ! Stranded non-wetting saturation in hysteresis case.
    PetscReal :: spnstrand
    ! Scanning curve Krn value
    PetscReal :: krnscan
  end type hy_auxvar_type

  type, public :: tl3_test_type
    PetscReal :: denotl,dengtl,viscotl,viscgtl,krotl,krgtl,krh
    PetscReal,pointer :: D_denotl(:),D_dengtl(:),D_viscotl(:),                 &
                         D_viscgtl(:),D_krotl(:),D_krgtl(:) 
    PetscReal,pointer :: D_krh(:)
  end type tl3_test_type

#ifdef tl_deriv_tests
    type, public ::  tl_auxvar_testing_type
    ! note stores many intermediate values as well to be tested
    PetscReal :: krotl,krgtl,viscotl,viscgtl,denotl,dengtl
    PetscReal :: krstl,viscstl,denstl
    PetscReal :: krom,krgm,krsm,krvm,kroi,krog,krow
    PetscReal :: fm,viso,viss,visg
    PetscReal :: uoil,uvap
    PetscReal :: cellpres
    PetscReal  :: krsi,krgi
    PetscReal  :: denos,dengs,denogs,denos_pre,denog

    PetscReal, pointer :: D_krotl(:),D_krgtl(:),D_viscotl(:),D_viscgtl(:)
    PetscReal, pointer :: D_denotl(:),D_dengtl(:)
    PetscReal, pointer :: D_krstl(:),D_viscstl(:),D_denstl(:)
    PetscReal, pointer :: D_krom(:),D_krgm(:),D_krsm(:),D_krvm(:),D_kroi(:)
    PetscReal, pointer :: D_krog(:),D_krow(:)
    PetscReal, pointer :: D_fm(:),D_viso(:),D_visg(:),D_viss(:)
    PetscReal, pointer :: D_uoil(:),D_uvap(:)
    PetscReal, pointer :: D_cellpres(:)
    PetscReal, pointer :: D_krsi(:),D_krgi(:)
    PetscReal, pointer :: D_denos(:),D_dengs(:),D_denogs(:),D_denos_pre(:),    &
                          D_denog(:)

  end type tl_auxvar_testing_type
#endif

#ifdef gw_deriv_tests
  type, public :: gw_auxvar_testing_type
    PetscReal :: wat_sat_pres, K_H_tilde, xgaseq

    PetscReal, pointer :: D_wat_sat_pres(:),D_K_H_tilde(:),D_xgaseq(:)
  end type gw_auxvar_testing_type
#endif

  public :: AuxVarTOWGStrip

contains

! ************************************************************************** !

subroutine AuxVarTOWGInit(this,option)
  ! 
  ! Initialize auxiliary object
  ! 
  ! Author: Paolo Orsini
  ! Date: 11/07/16
  ! 

  use Option_module

  implicit none
  
  class(auxvar_towg_type) :: this
  type(option_type) :: option

  call AuxVarBaseInit(this,option)

  this%istate_store = 0

#ifdef gw_deriv_tests
  !this%has_GW_test_object = PETSC_TRUE
  this%has_GW_test_object = PETSC_FALSE
  if (this%has_GW_test_object) then
    allocate(this%gwT)
     
    this%gwT%wat_sat_pres = 0.d0
    this%gwT%K_H_tilde = 0.d0
    this%gwT%xgaseq = 0.d0

    allocate(this%gwT%D_wat_sat_pres(option%nflowdof))
    this%gwT%D_wat_sat_pres = 0.d0
    allocate(this%gwT%D_K_H_tilde(option%nflowdof))
    this%gwT%D_K_H_tilde = 0.d0
    allocate(this%gwT%D_xgaseq(option%nflowdof))
    this%gwT%D_xgaseq = 0.d0

  endif
#endif

#ifdef tl_deriv_tests
  this%has_TL_test_object = PETSC_FALSE

  if (this%has_TL_test_object) then
    allocate(this%tlT)

    this%tlT%krotl = 0.d0
    this%tlT%krgtl = 0.d0
    this%tlT%viscotl = 0.d0
    this%tlT%viscgtl = 0.d0
    this%tlT%denotl = 0.d0
    this%tlT%dengtl = 0.d0

    this%tlT%krstl = 0.d0
    this%tlT%viscstl = 0.d0
    this%tlT%denstl = 0.d0

    this%tlT%krom = 0.d0
    this%tlT%krgm = 0.d0
    this%tlT%krsm = 0.d0
    this%tlT%krvm = 0.d0

    this%tlT%fm = 0.d0
    this%tlT%viso = 0.d0

    allocate(this%tlT%D_krotl(option%nflowdof))
    this%tlT%D_krotl = 0.d0
    allocate(this%tlT%D_krgtl(option%nflowdof))
    this%tlT%D_krgtl = 0.d0
    allocate(this%tlT%D_viscotl(option%nflowdof))
    this%tlT%D_viscotl = 0.d0
    allocate(this%tlT%D_viscgtl(option%nflowdof))
    this%tlT%D_viscgtl = 0.d0
    allocate(this%tlT%D_denotl(option%nflowdof))
    this%tlT%D_denotl = 0.d0
    allocate(this%tlT%D_dengtl(option%nflowdof))
    this%tlT%D_dengtl = 0.d0

    allocate(this%tlT%D_krstl(option%nflowdof))
    this%tlT%D_krstl = 0.d0
    allocate(this%tlT%D_viscstl(option%nflowdof))
    this%tlT%D_viscstl = 0.d0
    allocate(this%tlT%D_denstl(option%nflowdof))
    this%tlT%D_denstl = 0.d0

    allocate(this%tlT%D_krom(option%nflowdof))
    this%tlT%D_krom = 0.d0
    allocate(this%tlT%D_krgm(option%nflowdof))
    this%tlT%D_krgm = 0.d0
    allocate(this%tlT%D_krsm(option%nflowdof))
    this%tlT%D_krsm = 0.d0
    allocate(this%tlT%D_krvm(option%nflowdof))
    this%tlT%D_krvm = 0.d0
    allocate(this%tlT%D_kroi(option%nflowdof))
    this%tlT%D_kroi = 0.d0
    allocate(this%tlT%D_krog(option%nflowdof))
    this%tlT%D_krog = 0.d0
    allocate(this%tlT%D_krow(option%nflowdof))
    this%tlT%D_krow = 0.d0

    allocate(this%tlT%D_fm(option%nflowdof))
    this%tlT%D_fm = 0.d0
    allocate(this%tlT%D_viso(option%nflowdof))
    this%tlT%D_viso = 0.d0
    allocate(this%tlT%D_visg(option%nflowdof))
    this%tlT%D_visg = 0.d0
    allocate(this%tlT%D_viss(option%nflowdof))
    this%tlT%D_viss = 0.d0

    allocate(this%tlT%D_uoil(option%nflowdof))
    this%tlT%D_uoil = 0.d0
    allocate(this%tlT%D_uvap(option%nflowdof))
    this%tlT%D_uvap = 0.d0

    allocate(this%tlT%D_cellpres(option%nflowdof))
    this%tlT%D_cellpres= 0.d0

    allocate(this%tlT%D_krsi(option%nflowdof))
    this%tlT%D_krsi= 0.d0

    allocate(this%tlT%D_krgi(option%nflowdof))
    this%tlT%D_krgi= 0.d0

    allocate(this%tlT%D_denos(option%nflowdof))
    this%tlT%D_denos= 0.d0
    allocate(this%tlT%D_dengs(option%nflowdof))
    this%tlT%D_dengs= 0.d0
    allocate(this%tlT%D_denogs(option%nflowdof))
    this%tlT%D_denogs= 0.d0

    allocate(this%tlT%D_denos_pre(option%nflowdof))
    this%tlT%D_denos_pre= 0.d0

    allocate(this%tlT%D_denog(option%nflowdof))
    this%tlT%D_denog= 0.d0
  endif
#endif

end subroutine AuxVarTOWGInit

! ************************************************************************** !

subroutine InitTL(this,option)
  ! 
  ! Initialize auxiliary object or Todd Longstaff model
  ! 
  ! Author: Paolo Orsini
  ! Date: 07/06/17
  ! 

  use Option_module

  implicit none
  
  class(auxvar_towg_type) :: this
  type(option_type) :: option

  allocate(this%tl)

  this%tl%den_oil_eff_kg = 0.0
  this%tl%den_gas_eff_kg = 0.0

  if (.NOT. option%flow%numerical_derivatives) then
    this%tl%has_derivs = PETSC_TRUE
    this%tl%D_den_oil_eff_kg= 0.d0
    this%tl%D_den_gas_eff_kg= 0.d0
  endif

#ifdef tl3p_deriv_tests
    this%hastl3p_test_object = PETSC_FALSE
  if (option%flow%numerical_derivatives_compare) then
    this%hastl3p_test_object = PETSC_TRUE

    allocate(this%tl3TEST)

    nullify(this%tl3TEST%D_denotl)
    nullify(this%tl3TEST%D_dengtl)
    nullify(this%tl3TEST%D_viscotl)
    nullify(this%tl3TEST%D_viscgtl)
    nullify(this%tl3TEST%D_krotl)
    nullify(this%tl3TEST%D_krgtl)
    nullify(this%tl3TEST%D_krh)

    allocate(this%tl3TEST%D_denotl(option%nflowdof))
    this%tl3TEST%D_denotl= 0.d0
    allocate(this%tl3TEST%D_dengtl(option%nflowdof))
    this%tl3TEST%D_dengtl= 0.d0
    allocate(this%tl3TEST%D_viscotl(option%nflowdof))
    this%tl3TEST%D_viscotl= 0.d0
    allocate(this%tl3TEST%D_viscgtl(option%nflowdof))
    this%tl3TEST%D_viscgtl= 0.d0
    allocate(this%tl3TEST%D_krotl(option%nflowdof))
    this%tl3TEST%D_krotl= 0.d0
    allocate(this%tl3TEST%D_krgtl(option%nflowdof))
    this%tl3TEST%D_krgtl= 0.d0
    allocate(this%tl3TEST%D_krh(option%nflowdof))
    this%tl3TEST%D_krh= 0.d0
  endif
#endif

end subroutine InitTL

!--Routine to initialise the black oil substructure----------------------------

subroutine InitBO(this,option)

!------------------------------------------------------------------------------
! Initialises the black oil sub-structure of auxvars
! of auxvars. Contains the bubble point and the oil mole fractions.
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Sep 2017
!------------------------------------------------------------------------------

  use Option_module

  implicit none

  class(auxvar_towg_type) :: this
  type(option_type) :: option

  allocate(this%bo)

  this%bo%bubble_point=0.0
  this%bo%xg          =0.0
  this%bo%xo          =0.0

  if (.NOT. option%flow%numerical_derivatives) then
    this%bo%has_derivs= PETSC_TRUE
    this%bo%D_xo = 0.d0
    this%bo%D_xg = 0.d0
  else
    this%bo%has_derivs = PETSC_FALSE
  endif

end subroutine InitBO

!--Routine to initialise the compositional substructure------------------------

subroutine InitCO(this,option)

!------------------------------------------------------------------------------
! Initialises the compositional sub-structure of auxvars
! of auxvars. Contains the bubble point and the oil mole fractions.
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : 03/19/21
!------------------------------------------------------------------------------

  use Option_module

  implicit none

  class(auxvar_towg_type) :: this
  type(option_type) :: option

  ! Allocate 

  allocate(this%co)

  ! Values 

  allocate(this%co%xmf  (option%ncomp))
  allocate(this%co%ymf  (option%ncomp))
  allocate(this%co%amf  (option%ncomp))
  allocate(this%co%zmf  (option%ncomp))

  this%co%xmf   = 0.0d0
  this%co%ymf   = 0.0d0
  this%co%amf   = 0.0d0

  this%co%zmf   = 0.0d0

  this%co%diffg = 0.0

  this%co%v     = 0.0
  this%co%w     = 0.0

  this%co%mp_mdo  = 0.0
  this%co%mp_mdg  = 0.0
  this%co%mp_kgdo = 0.0
  this%co%mp_kgdg = 0.0

  ! Derivatives

  if (.NOT. option%flow%numerical_derivatives) then

    allocate(this%co%D_xmf(option%ncomp,option%nflowdof))
    allocate(this%co%D_ymf(option%ncomp,option%nflowdof))
    allocate(this%co%D_amf(option%ncomp,option%nflowdof))

    this%co%D_xmf = 0.0d0
    this%co%D_ymf = 0.0d0
    this%co%D_amf = 0.0d0

    allocate(this%co%D_diffg(option%nflowdof))

    this%co%D_diffg = 0.0

  endif

end subroutine InitCO

!--Routine to initialise the TOWG substructure-------------------------------

subroutine AuxVarTOWGStrip(this)
  ! 
  ! AuxVarTOWGStrip: Deallocates a towg auxiliary object
  ! 
  ! Author: Paolo Orsini
  ! Date: 10/30/16
  ! 
  use Utility_module, only : DeallocateArray

  implicit none

  class(auxvar_towg_type) :: this

  call AuxVarBaseStrip(this)

  if (associated(this%tl)) call this%StripTL()
  if (associated(this%bo)) call this%StripBO()
  if (associated(this%co)) call this%StripCO()
  if (associated(this%gw)) call this%StripGW()
  if (associated(this%hy)) call this%StripHY()

#ifdef gw_deriv_tests
  if (this%has_GW_test_object) then
    call DeallocateArray(this%gwT%D_wat_sat_pres)
    call DeallocateArray(this%gwT%D_K_H_tilde)
    call DeallocateArray(this%gwT%D_xgaseq)
  endif
#endif

#ifdef tl_deriv_tests
  if (this%has_TL_test_object) then
    call DeallocateArray(this%tlT%D_krotl)
    call DeallocateArray(this%tlT%D_krgtl)
    call DeallocateArray(this%tlT%D_viscotl)
    call DeallocateArray(this%tlT%D_viscgtl)
    call DeallocateArray(this%tlT%D_denotl)
    call DeallocateArray(this%tlT%D_dengtl)

    call DeallocateArray(this%tlT%D_krstl)
    call DeallocateArray(this%tlT%D_viscstl)
    call DeallocateArray(this%tlT%D_denstl)

    call DeallocateArray(this%tlT%D_krom)
    call DeallocateArray(this%tlT%D_krgm)
    call DeallocateArray(this%tlT%D_krsm)
    call DeallocateArray(this%tlT%D_krvm)
    call DeallocateArray(this%tlT%D_kroi)
    call DeallocateArray(this%tlT%D_krog)
    call DeallocateArray(this%tlT%D_krow)

    call DeallocateArray(this%tlT%D_fm)
    call DeallocateArray(this%tlT%D_viso)
    call DeallocateArray(this%tlT%D_visg)
    call DeallocateArray(this%tlT%D_viss)

    call DeallocateArray(this%tlT%D_uoil)
    call DeallocateArray(this%tlT%D_uvap)

    call DeallocateArray(this%tlT%D_cellpres)

    call DeallocateArray(this%tlT%D_krsi)
    call DeallocateArray(this%tlT%D_krgi)

    call DeallocateArray(this%tlT%D_denos)
    call DeallocateArray(this%tlT%D_dengs)
    call DeallocateArray(this%tlT%D_denogs)
    call DeallocateArray(this%tlT%D_denos_pre)
    call DeallocateArray(this%tlT%D_denog)
    deallocate(this%tlT)
  endif
#endif

end subroutine AuxVarTOWGStrip


! ************************************************************************** !

subroutine StripTL(this)
  ! 
  ! StripTL: Deallocates a the Todd Longstaff component of 
  !          the towg auxiliary object
  ! 
  ! Author: Paolo Orsini
  ! Date: 07/06/17
  ! 
  use Utility_module, only : DeallocateArray

  implicit none

  class(auxvar_towg_type) :: this

#ifdef tl3p_deriv_tests
  if (this%hastl3p_test_object) then
    call DeallocateArray(this%tl3TEST%D_denotl)
    call DeallocateArray(this%tl3TEST%D_dengtl)
    call DeallocateArray(this%tl3TEST%D_viscotl)
    call DeallocateArray(this%tl3TEST%D_viscotl)
    call DeallocateArray(this%tl3TEST%D_krotl)
    call DeallocateArray(this%tl3TEST%D_krgtl)
    call DeallocateArray(this%tl3TEST%D_krh)
  endif
#endif


  deallocate(this%tl)

end subroutine StripTL

!--Routine to strip the black oil substructure---------------------------------

subroutine StripBO(this)

!------------------------------------------------------------------------------
! Deallocate the black oil sub-structure of auxvars
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : Sep 2017
!------------------------------------------------------------------------------

  use Utility_module, only : DeallocateArray

  implicit none

  class(auxvar_towg_type) :: this

  deallocate(this%bo)

end subroutine StripBO

!--Routine to strip the compositional substructure-----------------------------

subroutine StripCO(this)

!------------------------------------------------------------------------------
! Deallocate the compositional sub-structure of auxvars
!------------------------------------------------------------------------------
! Author: Dave Ponting
! Date  : 19/03/21
!------------------------------------------------------------------------------

  use Utility_module, only : DeallocateArray

  implicit none

  class(auxvar_towg_type) :: this

  if( allocated(this%co%xmf  ) ) deallocate(this%co%xmf )
  if( allocated(this%co%ymf  ) ) deallocate(this%co%ymf )
  if( allocated(this%co%amf  ) ) deallocate(this%co%amf )

  if( allocated(this%co%zmf  ) ) deallocate(this%co%zmf )

  if( allocated(this%co%D_xmf) ) deallocate(this%co%D_xmf)
  if( allocated(this%co%D_ymf) ) deallocate(this%co%D_ymf)
  if( allocated(this%co%D_amf) ) deallocate(this%co%D_amf)

  if( allocated(this%co%D_diffg) ) deallocate(this%co%D_diffg)

  deallocate(this%Co)

end subroutine StripCO

! ************************************************************************** !

subroutine InitGW(this,option)

  !
  ! Allocate the GW data structure
  !
  ! Author: D Stone


  use Option_module

  implicit none

  class(auxvar_towg_type) :: this
  type(option_type) :: option

  allocate(this%gw)

  ! static allocation 
  this%gw%xmol =0.d0
  this%gw%diff=0.d0
  this%gw%pc_dkp=0.d0

  if (.NOT. option%flow%numerical_derivatives) then
    this%gw%has_derivs= PETSC_TRUE
    this%gw%D_xmol = 0.d0
    this%gw%D_diff = 0.d0
    this%gw%D_pc_dkp=0.d0
  else
    this%gw%has_derivs = PETSC_FALSE
  endif


end subroutine InitGW

! ************************************************************************** !

subroutine InitHY(this,option)
  !
  ! Allocate the hysteresis data structure
  !
  ! Author: Dave Ponting
  ! Date: 17/02/20
  !

  use Option_module

  implicit none

  class(auxvar_towg_type) :: this
  type(option_type) :: option

  allocate(this%hy)

  this%hy%spnmax    = 0.0
  this%hy%spnshft   = 0.0
  this%hy%spwshft   = 0.0
  this%hy%spntrap   = 0.0
  this%hy%spnstrand = 0.0
  this%hy%krnscan   = 0.0

end subroutine InitHY

! ************************************************************************** !

subroutine StripGW(this)

  !
  ! Release the GW data structure
  !
  ! Author: D Stone
  !

  use Utility_module, only : DeallocateArray

  implicit none

  class(auxvar_towg_type) :: this

  deallocate(this%gw)

end subroutine StripGW

! ************************************************************************** !

subroutine StripHY(this)

  !
  ! Release the hysteresis data structure
  !
  ! Author: Dave Ponting
  ! Date: 17/02/20
  !

  implicit none

  class(auxvar_towg_type) :: this

  if( associated(this%hy) ) then
    deallocate(this%hy)
    this%hy => null()
  endif

end subroutine StripHY

! ************************************************************************** !

end module AuxVars_TOWG_module


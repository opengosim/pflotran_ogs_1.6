## PFLOTRAN-OGS

PFLOTRAN-OGS is an open source reservoir simulator package with advanced capabilities for CO2 Storage. The software is supported and maintained by [opengosim](https://opengosim.com/).
For the documentation please visit [OGS-Wiki](https://docs.opengosim.com/), where you will find instructions for installation and tutorial to get started.
For any inquiry or to report any issue, please contact [OpenGoSim](https://opengosim.com/contact.php).